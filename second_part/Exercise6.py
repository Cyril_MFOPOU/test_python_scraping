from inspect import signature



class verified_attr_process(type):
    def __new__(cls, clsname, bases, attrs):
        has_attr_process = False
        for u,v in attrs.items():
            if(u == 'process' and len(signature(v).parameters) == 4): # in addition to self
                has_attr_process = True
        attrs["has_attr_process"] = has_attr_process
        return super(verified_attr_process, cls).__new__(cls, clsname, bases, attrs)



class inst_for_verification(metaclass=verified_attr_process):
    def process(self,arg1,arg2,arg3):
        pass

class inst_for_verification_2(metaclass=verified_attr_process):
    def process(self,arg1,arg2):
        pass


e = inst_for_verification()
print(e.has_attr_process)

f = inst_for_verification_2()
print(f.has_attr_process)