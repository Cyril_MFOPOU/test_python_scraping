import pytest

from second_part.Exercise6 import verified_attr_process,inst_for_verification,inst_for_verification_2


def test_meta_verified_attr_process():
    e = inst_for_verification()
    f = inst_for_verification_2()
    assert e.has_attr_process == True
    assert f.has_attr_process == False