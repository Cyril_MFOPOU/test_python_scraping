def exercise_one():
    for i in range(1,101):
        if i % 3 == 0 and i % 5 != 0:
            print('Three')
        elif i % 5 == 0 and i % 3 != 0:
            print('Five')
        elif i % 5 == 0 and i % 3 == 0:
            print('ThreeFive')
        else:
            print(i)

# Exercise 2
def colorful(number):
    if (number < 0):
        return 'Error,' + str(number) + ' is not positive.Please change the value of number'
    else:
        decomposition_list = []
        dict_product = {}
        number_str = str(number)
        #decomposition of number:  number = 263 , result : ['2', '26', '263', '6', '63', '3']
        for i in range(len(number_str)):
            for j in range(i, len(number_str)):
                decomposition_list.append(number_str[i:j + 1])
        #dictionary product of all consecutive subsets of digits. For the last list we've {0: 2, 1: 2x6, 2: 2x6x3, 3: 6, 4: 6x3, 5: 3}
        for p in range(len(decomposition_list)):
            t = 1
            x = decomposition_list[p]
            for j in range(len(x)):
                t *= int(x[j])
            dict_product[p] = t

        # final verification
        table_verification = []

        for m, n in dict_product.items():
            if n not in table_verification:
                table_verification.append(n)
        if len(table_verification) == len(decomposition_list):
            return str(number) + " is a colorful number"
        else:
            return str(number) + " isn't a colorful number"

# Exercise 3
def calculate(l):
    if isinstance(l, list):
        return sum([int(i) for i in l if type(i) == str and i.lstrip('-').isdigit()])
    return False

# Exercise 4
def anagrams(word,tab):
    list_anagrams = []
    for i in range(len(tab)):
        if(sorted(tab[i]) == sorted(word)):
            list_anagrams.append(tab[i])
    return list_anagrams

