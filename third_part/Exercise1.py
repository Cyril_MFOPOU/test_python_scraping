import requests
from bs4 import BeautifulSoup

def http_request_send():
    url = 'https://httpbin.org/anything'
    payload = {'isadmin' : 1}
    response = requests.post(url, params = payload)
    return response.text

def http_request_send_header():
    url = 'https://httpbin.org/anything'
    payload = {'isadmin': 1}
    new_agent = {'User-Agent': "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0"}
    response = requests.post(url, params=payload, headers=new_agent)
    return response.text



print(http_request_send())
#print(http_request_send_header())